CREATE TABLE todo_user (
                           id   BIGINT AUTO_INCREMENT PRIMARY KEY,
                           use_name VARCHAR(255) NOT NULL
);

CREATE TABLE todo_item (
                           id          BIGINT AUTO_INCREMENT PRIMARY KEY,
                           description VARCHAR(255) NOT NULL,
                           user_id     BIGINT NOT NULL,
                           event_date  timestamp NOT NULL,
                           CONSTRAINT fk_user
                               FOREIGN KEY (user_id)
                                   REFERENCES todo_user (id)
);
