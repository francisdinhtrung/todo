package com.li.lung.todo.service;

import com.li.lung.todo.domain.TodoItem;
import com.li.lung.todo.domain.User;
import com.li.lung.todo.repository.TodoItemRepository;
import com.li.lung.todo.repository.UserRepository;
import com.li.lung.todo.service.dto.TodoItemDTO;
import com.li.lung.todo.service.impl.TodoItemServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TodoItemServiceTest {

    @Mock
    private TodoItemRepository todoItemRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private TodoItemServiceImpl todoItemService;

    @BeforeEach
    void setup() {
        todoItemService = new TodoItemServiceImpl(userRepository, todoItemRepository, modelMapper);
    }
    @Test
    public void testCreateTodoItem() {
        // Arrange
        Instant eventDate = Instant.parse("2023-05-09T04:35:15.907Z");
        User user = new User(1L, "john");
        TodoItemDTO todoItemDTO = new TodoItemDTO();
        todoItemDTO.setDescription("Buy groceries");
        todoItemDTO.setEventDate(eventDate);

        TodoItem todoItem = new TodoItem(1L, "Buy groceries", eventDate , user);

        when(userRepository.findByUserName("john")).thenReturn(user);
        when(modelMapper.map(todoItemDTO, TodoItem.class)).thenReturn(todoItem);
        when(todoItemRepository.save(todoItem)).thenReturn(todoItem);
        when(modelMapper.map(todoItem, TodoItemDTO.class)).thenReturn(todoItemDTO);

        // Act
        TodoItemDTO result = todoItemService.createTodoItem("john", todoItemDTO);

        // Assert
        assertNotNull(result);
        assertEquals("Buy groceries", result.getDescription());
        assertNotNull(result.getEventDate());
        verify(userRepository, times(1)).findByUserName("john");
        verify(todoItemRepository, times(1)).save(any(TodoItem.class));
    }

    @Test
    public void testCreateTodoItem_UserNotFound() {
        // Arrange
        TodoItemDTO todoItemDTO = new TodoItemDTO();
        todoItemDTO.setDescription("Buy groceries");

        when(userRepository.findByUserName("john")).thenReturn(null);

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> todoItemService.createTodoItem("john", todoItemDTO));
        verify(userRepository, times(1)).findByUserName("john");
        verify(todoItemRepository, never()).save(any(TodoItem.class));
    }

    @Test
    public void testGetTodoItemsByUser() {
        // Arrange
        User user = new User(1L, "john");

        List<TodoItem> todoItems = new ArrayList<>();
        todoItems.add(new TodoItem(1L, "Buy groceries", Instant.parse("2023-05-09T04:35:15.907Z") ,user));
        todoItems.add(new TodoItem(2L, "Clean the house", Instant.parse("2023-05-10T04:35:15.907Z"), user));

        when(userRepository.findByUserName("john")).thenReturn(user);
        when(todoItemRepository.findByUser(user)).thenReturn(todoItems);
        when(modelMapper.map(any(TodoItem.class), eq(TodoItemDTO.class))).thenAnswer(invocation -> {
            TodoItem todoItem = invocation.getArgument(0);
            return new TodoItemDTO(todoItem.getId(),todoItem.getDescription(), todoItem.getEventDate());
        });

        // Act
        List<TodoItemDTO> result = todoItemService.getTodoItemsByUser("john");

        // Assert
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("Buy groceries", result.get(0).getDescription());
        assertEquals("Clean the house", result.get(1).getDescription());
        verify(userRepository, times(1)).findByUserName("john");
        verify(todoItemRepository, times(1)).findByUser(user);
    }

    @Test
    public void testGetTodoItemsByUser_UserNotFound() {
        // Arrange
        when(userRepository.findByUserName("john")).thenReturn(null);

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> todoItemService.getTodoItemsByUser("john"));
        verify(userRepository, times(1)).findByUserName("john");
        verify(todoItemRepository, never()).findByUser(any(User.class));
    }
}
