package com.li.lung.todo.service;

import com.li.lung.todo.domain.User;
import com.li.lung.todo.repository.UserRepository;
import com.li.lung.todo.service.dto.UserDTO;
import com.li.lung.todo.service.error.UserNameAlreadyUsedException;
import com.li.lung.todo.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        userService =  new UserServiceImpl(userRepository, modelMapper);
    }
    @Test
    public void testCreateUser_Success() {
        // Arrange
        UserDTO userDTO = new UserDTO();
        userDTO.setUserName("john");

        User user = new User(1L,"john");
        User savedUser = new User(1L,"john");
        UserDTO savedUserDTO = new UserDTO();
        savedUserDTO.setUserName("john");

        when(userRepository.findByUserName("john")).thenReturn(null);
        when(modelMapper.map(userDTO, User.class)).thenReturn(user);
        when(userRepository.save(user)).thenReturn(savedUser);
        when(modelMapper.map(savedUser, UserDTO.class)).thenReturn(savedUserDTO);

        // Act
        UserDTO result = userService.createUser(userDTO);

        // Assert
        assertNotNull(result);
        assertEquals("john", result.getUserName());
        verify(userRepository, times(1)).findByUserName("john");
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void testCreateUser_UsernameExists() {
        // Arrange
        UserDTO userDTO = new UserDTO();
        userDTO.setUserName("john");

        User existingUser = new User(1L,"john");

        when(userRepository.findByUserName("john")).thenReturn(existingUser);

        // Act & Assert
        assertThrows(UserNameAlreadyUsedException.class, () -> userService.createUser(userDTO));
        verify(userRepository, times(1)).findByUserName("john");
        verify(userRepository, never()).save(any(User.class));
    }
}

