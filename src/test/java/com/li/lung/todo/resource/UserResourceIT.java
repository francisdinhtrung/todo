package com.li.lung.todo.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.li.lung.todo.domain.User;
import com.li.lung.todo.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserResourceIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;
    @Test
    public void testCreateUser() throws Exception {
        // Arrange
        User user = new User();
        user.setUserName("john");

        // Act
        MvcResult mvcResult = mockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isCreated())
                .andReturn();

        // Assert
        String response = mvcResult.getResponse().getContentAsString();
        User createdUser = objectMapper.readValue(response, User.class);
        assertThat(createdUser).isNotNull();
        assertThat(createdUser.getUserName()).isEqualTo("john");

        // Verify in the database
        User savedUser = userRepository.findByUserName("john");
        assertThat(savedUser).isNotNull();
        assertThat(savedUser.getUserName()).isEqualTo("john");
    }

    @Test
    public void testCreateExistedUser() throws Exception {
        // Arrange
        User user = new User();
        user.setUserName("john01");
        userRepository.save(user);
        // Act
        MvcResult mvcResult = mockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isBadRequest())
                .andReturn();

        // Assert
        String response = mvcResult.getResponse().getContentAsString();
        assertThat(response).contains("Username already exists.");
    }
}
