package com.li.lung.todo.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.li.lung.todo.domain.TodoItem;
import com.li.lung.todo.domain.User;
import com.li.lung.todo.repository.TodoItemRepository;
import com.li.lung.todo.repository.UserRepository;
import com.li.lung.todo.service.dto.TodoItemDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.Instant;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoItemResourceIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TodoItemRepository todoItemRepository;

    @Autowired
    private UserRepository userRepository;


    @Test
    public void testCreateTodoItem() throws Exception {
        // Arrange
        User user = new User();
        user.setUserName("john");
        userRepository.save(user);

        Instant eventDate = Instant.parse("2023-05-09T04:35:15.907Z");
        TodoItemDTO todoItemDTO = new TodoItemDTO();
        todoItemDTO.setDescription("Buy groceries");
        todoItemDTO.setEventDate(eventDate);

        // Act
        MvcResult mvcResult = mockMvc.perform(post("/api/todo-items/john")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(todoItemDTO)))
                .andExpect(status().isCreated())
                .andReturn();

        // Assert
        String response = mvcResult.getResponse().getContentAsString();
        TodoItemDTO responseTodoItem = objectMapper.readValue(response, TodoItemDTO.class);
        assertThat(responseTodoItem).isNotNull();
        assertNotNull(responseTodoItem.getId());
        assertThat(responseTodoItem.getDescription()).isEqualTo("Buy groceries");
        assertNotNull(responseTodoItem.getEventDate());

        List<TodoItem> todoItems = todoItemRepository.findByUser(user);
        assertThat(todoItems).hasSize(1);
        TodoItem savedTodoItem = todoItems.get(0);
        assertThat(savedTodoItem.getDescription()).isEqualTo("Buy groceries");
    }

    @Test
    public void testGetTodoItemsByUser() throws Exception {
        // Arrange
        User user = new User();
        user.setUserName("john01");
        userRepository.save(user);

        TodoItem todoItem = new TodoItem();
        todoItem.setDescription("Buy groceries");
        todoItem.setEventDate(Instant.now());
        todoItem.setUser(user);
        todoItemRepository.save(todoItem);

        // Act
        MvcResult mvcResult = mockMvc.perform(get("/api/todo-items/john01"))
                .andExpect(status().isOk())
                .andReturn();

        // Assert
        String response = mvcResult.getResponse().getContentAsString();
        List<TodoItemDTO> responseTodoItems = objectMapper.readValue(response, objectMapper.getTypeFactory().constructCollectionType(List.class, TodoItemDTO.class));
        assertThat(responseTodoItems).hasSize(1);
        TodoItemDTO responseTodoItem = responseTodoItems.get(0);
        assertThat(responseTodoItem.getDescription()).isEqualTo("Buy groceries");
        assertNotNull(responseTodoItem.getEventDate());
    }
}
