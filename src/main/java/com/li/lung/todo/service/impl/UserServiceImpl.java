package com.li.lung.todo.service.impl;

import com.li.lung.todo.domain.User;
import com.li.lung.todo.repository.UserRepository;
import com.li.lung.todo.service.UserService;
import com.li.lung.todo.service.dto.UserDTO;
import com.li.lung.todo.service.error.UserNameAlreadyUsedException;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDTO createUser(UserDTO userDTO) {
        User existingUser = userRepository.findByUserName(userDTO.getUserName());
        if (existingUser != null) {
            log.error("UserName = {} already exists.", userDTO.getUserName());
            throw new UserNameAlreadyUsedException("Username already exists.");
        }

        User user = modelMapper.map(userDTO, User.class);
        User savedUser = userRepository.save(user);
        return modelMapper.map(savedUser, UserDTO.class);
    }
}
