package com.li.lung.todo.service;

import com.li.lung.todo.service.dto.UserDTO;

public interface UserService {
    UserDTO createUser(UserDTO userDTO);
}
