package com.li.lung.todo.service.dto;

import lombok.*;

import java.time.Instant;

@Data
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TodoItemDTO {
    private Long id;
    private String description;
    private Instant eventDate;
}
