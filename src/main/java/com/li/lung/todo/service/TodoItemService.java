package com.li.lung.todo.service;

import com.li.lung.todo.service.dto.TodoItemDTO;

import java.util.List;

public interface TodoItemService {
    TodoItemDTO createTodoItem(String username, TodoItemDTO todoItemDTO);
    List<TodoItemDTO> getTodoItemsByUser(String username);
}
