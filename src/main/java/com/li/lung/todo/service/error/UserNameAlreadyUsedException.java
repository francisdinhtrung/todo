package com.li.lung.todo.service.error;

public class UserNameAlreadyUsedException extends RuntimeException {
    public UserNameAlreadyUsedException(String message){
        super(message);
    }
}
