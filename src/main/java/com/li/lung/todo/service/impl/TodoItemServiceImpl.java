package com.li.lung.todo.service.impl;

import com.li.lung.todo.domain.TodoItem;
import com.li.lung.todo.domain.User;
import com.li.lung.todo.repository.TodoItemRepository;
import com.li.lung.todo.repository.UserRepository;
import com.li.lung.todo.service.TodoItemService;
import com.li.lung.todo.service.dto.TodoItemDTO;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class TodoItemServiceImpl implements TodoItemService {

    private final Logger log = LoggerFactory.getLogger(TodoItemServiceImpl.class);
    private final UserRepository userRepository;
    private final TodoItemRepository todoItemRepository;
    private final ModelMapper modelMapper;

    public TodoItemServiceImpl(UserRepository userRepository, TodoItemRepository todoItemRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.todoItemRepository = todoItemRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public TodoItemDTO createTodoItem(String username, TodoItemDTO todoItemDTO) {

        User user = userRepository.findByUserName(username);
        if (user == null) {
            log.error("UserName = {} not found", username);
            throw new IllegalArgumentException("User not found.");
        }

        TodoItem todoItem = modelMapper.map(todoItemDTO, TodoItem.class);
        todoItem.setUser(user);
        TodoItem savedTodoItem = todoItemRepository.save(todoItem);
        return modelMapper.map(savedTodoItem, TodoItemDTO.class);
    }

    @Override
    public List<TodoItemDTO> getTodoItemsByUser(String username) {
        User user = userRepository.findByUserName(username);
        if (user == null) {
            throw new IllegalArgumentException("User not found.");
        }

        List<TodoItem> todoItems = todoItemRepository.findByUser(user);
        return todoItems.stream()
                .map(todoItem -> modelMapper.map(todoItem, TodoItemDTO.class))
                .collect(Collectors.toList());
    }
}
