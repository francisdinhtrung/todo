package com.li.lung.todo.domain;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@Table(name = "todo_user")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name", nullable = false)
    private String userName;
}
