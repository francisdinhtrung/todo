package com.li.lung.todo.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.Instant;

@Table(name = "todo_item")
@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TodoItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private Instant eventDate;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
}
