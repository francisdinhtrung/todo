package com.li.lung.todo.resource;

import com.li.lung.todo.service.TodoItemService;
import com.li.lung.todo.service.dto.TodoItemDTO;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TodoItemResource {

    private final TodoItemService todoItemService;
    public TodoItemResource(TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @PostMapping("/todo-items/{username}")
    public ResponseEntity<TodoItemDTO> createTodoItem(@PathVariable("username") String username, @RequestBody TodoItemDTO todoItemDTO) {
        TodoItemDTO savedTodoItemDTO = todoItemService.createTodoItem(username, todoItemDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedTodoItemDTO);
    }

    @GetMapping("/todo-items/{username}")
    public ResponseEntity<List<TodoItemDTO>> getTodoItemsByUser(@PathVariable("username") String username) {
        List<TodoItemDTO> todoItemsDTO = todoItemService.getTodoItemsByUser(username);
        return ResponseEntity.ok(todoItemsDTO);
    }
}
